package speedtest

import (
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
	"time"
)

const (
	cloudflareUpload   = "https://speed.cloudflare.com/__up"
	cloudflareDownload = "https://speed.cloudflare.com/__down"
	measureId          = "1234567890"
)

var uploadURL *url.URL
var downloadURL *url.URL

func init() {
	var err error
	downloadURL, err = url.Parse(cloudflareDownload)
	if err != nil {
		panic("couldn't parse download URL")
	}
	uploadURL, err = url.Parse(cloudflareUpload)
	if err != nil {
		panic("couldn't parse upload URL")
	}
}

// Download bytes from cloudflare download server
func DownloadTest(bytes int) (time.Duration, error) {
	url := *downloadURL
	q := url.Query()
	q.Set("bytes", strconv.Itoa(bytes))
	//	q.Set("measId", measureId)
	url.RawQuery = q.Encode()
	start := time.Now()
	c := http.Client{
		Timeout: time.Minute,
	}
	resp, err := c.Get(url.String())
	if err != nil {
		return time.Since(start), err
	}
	defer resp.Body.Close()
	_, err = io.Copy(ioutil.Discard, resp.Body)
	return time.Since(start), err
}

func LatencyTest() (time.Duration, error) {
	url := *downloadURL
	c := http.Client{
		Timeout: time.Second * 5,
	}
	start := time.Now()
	resp, err := c.Get(url.String())
	if err != nil {
		return time.Since(start), err
	}
	defer resp.Body.Close()
	_, err = io.Copy(ioutil.Discard, resp.Body)
	return time.Since(start), err
}

// Given a time and number of bytes, return MBits/s
func CalcBandwidth(d time.Duration, b int) float64 {
	bps := float64(b) / d.Seconds()
	return (bps / 1000000) * 8
}
