package main

import (
	"fmt"
	"time"

	"gitlab.com/annonymouse/speedtest"
)

func main() {
	for {
		d, err := speedtest.LatencyTest()
		if err != nil {
			fmt.Printf("%s, 0 [%v]\n", time.Now().Format("2006/01/02 15:04:05"), err)
		} else {
			fmt.Printf("%s, %d\n", time.Now().Format("2006/01/02 15:04:05"), d.Milliseconds())
		}
		time.Sleep(time.Second * 5)
	}
}
