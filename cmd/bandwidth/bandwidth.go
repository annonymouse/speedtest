package main

import (
	"flag"
	"fmt"
	"sync"
	"time"

	"gitlab.com/annonymouse/speedtest"
)

const (
	bytes25MB  = 25000000
	bytes10MB  = 10000000
	bytes1MB   = 1000000
	bytes100KB = 100000
)

var (
	repeat time.Duration
)

func init() {
	flag.DurationVar(&repeat, "repeat", 0, "frequency to repeat")
}

type bwTest struct {
	bw int
	n  int
}

func main() {
	flag.Parse()
	tests := []bwTest{
		{bytes100KB, 10},
		{bytes1MB, 100},
		{bytes10MB, 10},
		{bytes25MB, 4},
	}
	for {
		for _, t := range tests {
			wg := sync.WaitGroup{}
			start := time.Now()
			var oneError error
			once := sync.Once{}
			for i := 0; i < t.n; i++ {
				wg.Add(1)
				go func() {
					defer wg.Done()
					_, err := speedtest.DownloadTest(t.bw)
					once.Do(func() {
						oneError = err
					})
					//				fmt.Printf("download: %d, took: %v, bw:%fMB/s [%v]\n", bw, d, calcBW(d, bw), err)
				}()
			}
			wg.Wait()
			d := time.Since(start)
			bw := t.bw * t.n
			if oneError != nil {
				bw = 0
			}
			fmt.Printf("%s, %f, %d, %f\n", start.Format("2006/01/02 15:04:05"), speedtest.CalcBandwidth(d, bw), bw, d.Seconds())
		}
		if repeat == 0 {
			return
		}
		time.Sleep(repeat)
	}
}
